	logger = system.util.getLogger("WebDev Get Alarms")
#	logger.info(': )')
	# logger.info('tag_name: ' + str(type(tag_name)))
    # system.alarm.queryStatus(priority, state, path, source, displaypath, all_properties, any_properties, defined, includeShelved)
	alarms = system.alarm.queryStatus(State=["ActiveUnacked","ActiveAcked","ClearAcked","ClearUnacked"], includeShelved="false")
	result = {}
	columns = ['Active', 'Acknowledged', 'Active Time Elapsed', 'Tag Name', 'Constraint','Id', 'Shelved']		
	rows = []
	for alarm in alarms:
		# logger.info(str(alarm.getSource()) + ': ' + str(alarm.getLastEventState()))
 		row = []
 		tag_path = alarm.getDisplayPathOrSource() 
		tag_name = tag_path.split("/")[0]
		state = alarm.getState()
  		escalated = system.tag.read(tag_name + "/escalate")
  		ignored = system.tag.read(tag_name + "/ignore")
 		row.append(state.isActive()) # alarming
 		row.append(state.isAcked()) # acknowledged
 		row.append(system.date.fromMillis(alarm.getActiveData().getTimestamp())) # when_alarmed
  		row.append(tag_path) # who
  		row.append(str(alarm.getActiveData().getRawValueMap())) # why_alarming
 		row.append(alarm.getId()) # uuid
 		row.append(alarm.isShelved()) # shelved
 		row.append(escalated.value) # escalated
		if alarm.getClearedData() is None:
			clear_time = "(na)"
			row.append(clear_time) # when_cleared
		else:
			clear_time = system.date.fromMillis(alarm.getClearedData().getTimestamp())
			row.append(clear_time) # when_cleared
 		row.append(ignored.value) # ignored
		rows.append(row)
	shelved_paths = system.alarm.getShelvedPaths()
	shelved_paths_for_js = []
	for shelved_path in shelved_paths:
#		logger.info('type shelved_path: ' + str(type(shelved_path)))
#		logger.info('str shelved_path.getPath: ' + str(shelved_path.getPath()))
		shelved_path_for_js = {}
		shelved_path_for_js['who'] = str(shelved_path.getPath().toStringSimple())
		shelved_path_for_js['expired'] = str(shelved_path.isExpired())
#		logger.info('shelved_path_for_js: ' + str(shelved_path_for_js))
		shelved_paths_for_js.append(shelved_path_for_js)
	result['rows'] = rows
	result['columns'] = columns
	result['shelved_tags'] = shelved_paths_for_js
	result['KWOPC_state'] = system.opc.getServerState("KWOPC")
	return {'json': result}