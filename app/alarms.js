const debug = require('debug')('tags')
console.log("DEBUG: " + process.env.DEBUG);

var config = require('../config.js').get(process.env.NODE_ENV);
console.log("NODE_ENV: " + process.env.NODE_ENV);
const http = require("http");
const querystring = require("querystring");

var ignition_machine = config['ignition_machine'];
console.log("ignition_machine: " + ignition_machine);
var ignition_port = 8088;
var ignition_alarms_path  = "/main/system/webdev/alarms/alarms";
var ignition_ignore_path  = "/main/system/webdev/alarms/ignore";
var ignition_shelve_path  = "/main/system/webdev/alarms/shelve";
var ignition_escalate_path  = "/main/system/webdev/alarms/escalate";
var ignition_deescalate_path  = "/main/system/webdev/alarms/deescalate";
var ignition_history_path = "/main/system/webdev/alarms/history";
var ignition_alarms_uri  = "http://"+ignition_machine+":"+ignition_port+ignition_alarms_path; 
var ignition_history_uri = "http://"+ignition_machine+":"+ignition_port+ignition_history_path; 
var unacknowledged_alarms = false; 
var locations = {
    'com': { 'lat': 30.285635, 'lng': -97.738531 },
    'crb': { 'lat': 30.282169, 'lng': -97.726468 },
    'aca': { 'lat': 30.2877, 'lng': -97.7357 },
    'adh': { 'lat': 30.2915, 'lng': -97.7406 },
    'afp': { 'lat': 30.2853, 'lng': -97.7263 },
    'ahg': { 'lat': 30.2885, 'lng': -97.7377 },
    'anb': { 'lat': 30.2783, 'lng': -97.731 },
    'and': { 'lat': 30.2882, 'lng': -97.7398 },
    'arc': { 'lat': 30.291755, 'lng': -97.736252 },
    'art': { 'lat': 30.2862, 'lng': -97.733 },
    'att': { 'lat': 30.282, 'lng': -97.7405 },
    'bat': { 'lat': 30.2848, 'lng': -97.7389 },
    'bel': { 'lat': 30.2837, 'lng': -97.7337 },
    'ben': { 'lat': 30.284, 'lng': -97.739 },
    'bgh': { 'lat': 30.2869, 'lng': -97.7388 },
    'bhd': { 'lat': 30.2831, 'lng': -97.736 },
    'bio': { 'lat': 30.2872, 'lng': -97.7398 },
    'bld': { 'lat': 30.2887, 'lng': -97.7394 },
    'bma': { 'lat': 30.281, 'lng': -97.7374 },
    'bmc': { 'lat': 30.2902, 'lng': -97.7408 },
    'bme': { 'lat': 30.2892, 'lng': -97.7386 },
    'bot': { 'lat': 30.287, 'lng': -97.74 },
    'brb': { 'lat': 30.2852, 'lng': -97.7367 },
    'brg': { 'lat': 30.2809, 'lng': -97.7362 },
    'bsb': { 'lat': 30.2814, 'lng': -97.7354 },
    'btl': { 'lat': 30.2854, 'lng': -97.7403 },
    'bur': { 'lat': 30.2888, 'lng': -97.7385 },
    'bwy': { 'lat': 30.2908, 'lng': -97.7382 },
    'cal': { 'lat': 30.2845, 'lng': -97.7402 },
    'cba': { 'lat': 30.2842, 'lng': -97.7378 },
    'ccg': { 'lat': 30.2821, 'lng': -97.7404 },
    'ccj': { 'lat': 30.2881, 'lng': -97.7304 },
    'cda': { 'lat': 30.283, 'lng': -97.7257 },
    'cdl': { 'lat': 30.2788, 'lng': -97.733 },
    'cee': { 'lat': 30.2906, 'lng': -97.7364 },
    'cla': { 'lat': 30.2849, 'lng': -97.7354 },
    'clk': { 'lat': 30.2817, 'lng': -97.7349 },
    'cma': { 'lat': 30.2894, 'lng': -97.7407 },
    'cmb': { 'lat': 30.2892, 'lng': -97.7411 },
    'cml': { 'lat': 30.2829, 'lng': -97.7253 },
    'cpe': { 'lat': 30.2903, 'lng': -97.7361 },
    'crd': { 'lat': 30.2887, 'lng': -97.7401 },
    'crh': { 'lat': 30.2885, 'lng': -97.7334 },
    'cs3': { 'lat': 30.2807, 'lng': -97.7355 },
    'cs4': { 'lat': 30.2887, 'lng': -97.7325 },
    'cs5': { 'lat': 30.2907, 'lng': -97.7355 },
    'cs6': { 'lat': 30.2864, 'lng': -97.7358 },
    'csa': { 'lat': 30.2885, 'lng': -97.7357 },
    'ct1': { 'lat': 30.2866, 'lng': -97.7348 },
    'dcp': { 'lat': 30.2762, 'lng': -97.733 },
    'dev': { 'lat': 30.2873, 'lng': -97.7236 },
    'dfa': { 'lat': 30.2859, 'lng': -97.7318 },
    'dff': { 'lat': 30.2794, 'lng': -97.7265 },
    'dtb': { 'lat': 30.2873, 'lng': -97.7322 },
    'eas': { 'lat': 30.2811, 'lng': -97.7382 },
    'ecg': { 'lat': 30.279730, 'lng': -97.727907 },
    'ecj': { 'lat': 30.289, 'lng': -97.7354 },
    'eer': { 'lat': 30.288012, 'lng': -97.73515 },
    'ens': { 'lat': 30.2881, 'lng': -97.7353 },
    'eps': { 'lat': 30.2858, 'lng': -97.7367 },
    'erc': { 'lat': 30.2769, 'lng': -97.7322 },
    'etc': { 'lat': 30.2899, 'lng': -97.7354 },
    'fac': { 'lat': 30.2863, 'lng': -97.7404 },
    'fc1': { 'lat': 30.2846, 'lng': -97.7226 },
    'fc2': { 'lat': 30.2839, 'lng': -97.7231 },
    'fc3': { 'lat': 30.2848, 'lng': -97.7237 },
    'fc4': { 'lat': 30.2843, 'lng': -97.7236 },
    'fc5': { 'lat': 30.2836, 'lng': -97.7246 },
    'fc6': { 'lat': 30.2834, 'lng': -97.7258 },
    'fc7': { 'lat': 30.2833, 'lng': -97.7264 },
    'fc8': { 'lat': 30.2854, 'lng': -97.7239 },
    'fc9': { 'lat': 30.2838, 'lng': -97.724 },
    'fct': { 'lat': 30.2843, 'lng': -97.7224 },
    'fdh': { 'lat': 30.2894, 'lng': -97.7325 },
    'fnt': { 'lat': 30.2879, 'lng': -97.7379 },
    'fpc': { 'lat': 30.2794, 'lng': -97.725 },
    'gar': { 'lat': 30.2852, 'lng': -97.7385 },
    'gdc': { 'lat': 30.2863, 'lng': -97.7365 },
    'gea': { 'lat': 30.2877, 'lng': -97.7392 },
    'geb': { 'lat': 30.2863, 'lng': -97.7386 },
    'gol': { 'lat': 30.2854, 'lng': -97.7412 },
    'grc': { 'lat': 30.2838, 'lng': -97.7359 },
    'gre': { 'lat': 30.284, 'lng': -97.7364 },
    'grf': { 'lat': 30.2842, 'lng': -97.7356 },
    'grg': { 'lat': 30.2877, 'lng': -97.7399 },
    'grp': { 'lat': 30.2844, 'lng': -97.7359 },
    'grs': { 'lat': 30.2836, 'lng': -97.736 },
    'gsb': { 'lat': 30.2842, 'lng': -97.7383 },
    'gug': { 'lat': 30.2795, 'lng': -97.7432 },
    'hdb': { 'lat': 30.27775, 'lng': -97.7348 },
    'hlb': { 'lat': 30.2754, 'lng': -97.73346 },
    'hma': { 'lat': 30.2869, 'lng': -97.7406 },
    'hrc': { 'lat': 30.2843, 'lng': -97.7412 },
    'hrh': { 'lat': 30.2842, 'lng': -97.7402 },
    'hsm': { 'lat': 30.2889, 'lng': -97.7408 },
    'htb': { 'lat': 30.27742, 'lng': -97.73511 },
    'icb': { 'lat': 30.3148, 'lng': -97.7278 },
    'int': { 'lat': 30.2884, 'lng': -97.7434 },
    'ipf': { 'lat': 30.2863, 'lng': -97.7265 },
    'jcd': { 'lat': 30.2822, 'lng': -97.7363 },
    'jes': { 'lat': 30.2829, 'lng': -97.7368 },
    'jgb': { 'lat': 30.2859, 'lng': -97.7357 },
    'jhh': { 'lat': 30.2784, 'lng': -97.732 },
    'jjf': { 'lat': 30.2835, 'lng': -97.7325 },
    'jon': { 'lat': 30.2886, 'lng': -97.7317 },
    'kin': { 'lat': 30.2904, 'lng': -97.7396 },
    'lbj': { 'lat': 30.2858, 'lng': -97.7292 },
    'lch': { 'lat': 30.2886, 'lng': -97.7408 },
    'ldh': { 'lat': 30.2826, 'lng': -97.7359 },
    'lfh': { 'lat': 30.2881, 'lng': -97.7408 },
    'lla': { 'lat': 30.2906, 'lng': -97.7405 },
    'llb': { 'lat': 30.2909, 'lng': -97.7405 },
    'llc': { 'lat': 30.2911, 'lng': -97.7405 },
    'lld': { 'lat': 30.2906, 'lng': -97.7409 },
    'lle': { 'lat': 30.2909, 'lng': -97.741 },
    'llf': { 'lat': 30.2911, 'lng': -97.7408 },
    'ltd': { 'lat': 30.2893, 'lng': -97.7397 },
    'lth': { 'lat': 30.286, 'lng': -97.7352 },
    'mag': { 'lat': 30.2828, 'lng': -97.7309 },
    'mai': { 'lat': 30.286, 'lng': -97.7394 },
    'mbb': { 'lat': 30.2885, 'lng': -97.7373 },
    'mez': { 'lat': 30.2844, 'lng': -97.7389 },
    'mfh': { 'lat': 30.282, 'lng': -97.7311 },
    'mhd': { 'lat': 30.2836, 'lng': -97.7353 },
    'mms': { 'lat': 30.2826, 'lng': -97.7301 },
    'mnc': { 'lat': 30.2823, 'lng': -97.7327 },
    'mrh': { 'lat': 30.2873, 'lng': -97.7309 },
    'mbe': { 'lat': 30.2873, 'lng': -97.7309 },
    'msb': { 'lat': 30.2827, 'lng': -97.7257 },
    'nez': { 'lat': 30.2846, 'lng': -97.7325 },
    'nhb': { 'lat': 30.2876, 'lng': -97.738 },
    'nms': { 'lat': 30.2892, 'lng': -97.7376 },
    'noa': { 'lat': 30.2911, 'lng': -97.7375 },
    'nur': { 'lat': 30.2777, 'lng': -97.7336 },
    'pac': { 'lat': 30.2864, 'lng': -97.7311 },
    'pai': { 'lat': 30.287, 'lng': -97.7387 },
    'par': { 'lat': 30.2848, 'lng': -97.7402 },
    'pat': { 'lat': 30.288, 'lng': -97.7364 },
    'pcl': { 'lat': 30.2828, 'lng': -97.7382 },
    'phd': { 'lat': 30.2824, 'lng': -97.7351 },
    'phr': { 'lat': 30.2882, 'lng': -97.7386 },
    'pob': { 'lat': 30.2869, 'lng': -97.7366 },
    'ppa': { 'lat': 30.2869, 'lng': -97.7344 },
    'ppb': { 'lat': 30.2813, 'lng': -97.7269 },
    'ppe': { 'lat': 30.2869, 'lng': -97.7359 },
    'ppl': { 'lat': 30.2865, 'lng': -97.7352 },
    'rhd': { 'lat': 30.2831, 'lng': -97.7351 },
    'rlm': { 'lat': 30.2889, 'lng': -97.7363 },
    'rrh': { 'lat': 30.282138, 'lng': -97.741417 },
    'rsc': { 'lat': 30.2815, 'lng': -97.7324 },
    'sac': { 'lat': 30.2849, 'lng': -97.7363 },
    'sag': { 'lat': 30.2887, 'lng': -97.7427 },
    'sbs': { 'lat': 30.2805, 'lng': -97.725 },
    'sea': { 'lat': 30.29, 'lng': -97.7373 },
    'ser': { 'lat': 30.2877, 'lng': -97.7346 },
    'sjg': { 'lat': 30.2877, 'lng': -97.7329 },
    'sjh': { 'lat': 30.2823, 'lng': -97.7344 },
    'sof': { 'lat': 30.2809, 'lng': -97.7275 },
    'srh': { 'lat': 30.285, 'lng': -97.7289 },
    'ssb': { 'lat': 30.2901, 'lng': -97.7384 },
    'ssw': { 'lat': 30.2806, 'lng': -97.7327 },
    'std': { 'lat': 30.2836, 'lng': -97.7323 },
    'sut': { 'lat': 30.285, 'lng': -97.7408 },
    'sw7': { 'lat': 30.2908, 'lng': -97.7363 }, 
    'swg': { 'lat': 30.2912, 'lng': -97.7371 },
    'szb': { 'lat': 30.2817, 'lng': -97.7388 },
    'tcc': { 'lat': 30.287, 'lng': -97.729 },
    'tmm': { 'lat': 30.287, 'lng': -97.7324 },
    'tnh': { 'lat': 30.2886, 'lng': -97.7307 },
    'trg': { 'lat': 30.2791, 'lng': -97.7339 },
    'tsb': { 'lat': 30.3152, 'lng': -97.7267 },
    'tsc': { 'lat': 30.2799, 'lng': -97.7335 },
    'tsg': { 'lat': 30.2913, 'lng': -97.7386 },
    'ttc': { 'lat': 30.281288, 'lng': -97.726536 },
    'ua9': { 'lat': 30.2903, 'lng': -97.7387 },
    'uil': { 'lat': 30.2833, 'lng': -97.7236 },
    'unb': { 'lat': 30.2866, 'lng': -97.7411 },
    'upb': { 'lat': 30.2841, 'lng': -97.7304 },
    'uss': { 'lat': 30.2926, 'lng': -97.7363 },
    'uta': { 'lat': 30.2793, 'lng': -97.7428 },
    'utc': { 'lat': 30.2831, 'lng': -97.7388 },
    'utx': { 'lat': 30.2843, 'lng': -97.7344 },
    'wag': { 'lat': 30.2851, 'lng': -97.7376 },
    'wat': { 'lat': 30.2784, 'lng': -97.7336 },
    'wch': { 'lat': 30.2861, 'lng': -97.7384 },
    'wel': { 'lat': 30.2866, 'lng': -97.7377 },
    'win': { 'lat': 30.2859, 'lng': -97.7345 },
    'wmb': { 'lat': 30.2854, 'lng': -97.7406 },
    'glt': { 'lat': 30.2875, 'lng': -97.7359 },
    'wwh': { 'lat': 30.2893, 'lng': -97.7418 },
};
var full_names = {
    adh: 'ALMETRIS DUREN RESIDENCE HALL',
    ahg: 'ANNA HISS GYMNASIUM',
    anb: 'ARNO NOWOTNY BUILDING',
    and: 'ANDREWS DORMITORY',
    arc: 'ANIMAL RESOURCES CENTER',
    art: 'ART BUILDING AND MUSEUM',
    att: 'AT&T EXECUTIVE EDUC & CONF CENTER',
    bat: 'BATTS HALL',
    bel: 'L. THEO BELLMONT HALL',
    ben: 'BENEDICT HALL',
    bhd: 'BRACKENRIDGE HALL DORM',
    bio: 'BIOLOGICAL LABORATORIES',
    bld: 'BLANTON DORMITORY',
    bma: 'JACK S. BLANTON MUSEUM OF ART',
    bmc: 'BELO CENTER FOR NEW MEDIA',
    bme: 'BIOMEDICAL ENGINEERING BUILDING',
    brb: 'BERNARD AND AUDRE RAPOPORT BUILDING',
    brg: 'BRAZOS GARAGE',
    btl: 'BATTLE HALL',
    bur: 'BURDINE HALL',
    bwy: 'BWY',
    cal: 'CALHOUN HALL',
    cba: 'COLLEGE OF BUSINESS ADMINISTRATION',
    ccj: 'CONNALLY CENTER FOR JUSTICE',
    cdl: 'COLLECTIONS DEPOSIT LIBRARY',
    cla: 'LIBERAL ARTS BUILDING',
    cma: 'JESSE H. JONES COMM. CTR. (BLDG. A)',
    cmb: 'JESSE H. JONES COMM. CTR. (BLDG. B)',
    com: 'COMPUTATION CENTER',
    cp1: 'CHILLING PLANT #1',
    cpe: 'CHEMICAL AND PETROLEUM ENGINEERING',
    crb: 'COMPUTATIONAL RESOURCE BUILDING',
    crd: 'CAROTHERS DORMITORY',
    crh: 'CREEKSIDE RESIDENCE HALL',
    cs3: 'CENTRAL CHILLING STATION NO. 3',
    cs4: 'CENTRAL CHILLING STATION NO. 4',
    cs5: 'CENTRAL CHILLING STATION NO. 5',
    cs6: 'CENTRAL CHILLING STATION NO. 6',
    cs7: 'CENTRAL CHILLING STATION NO. 7',
    dcp: 'DENTON A. COOLEY PAVILION',
    dfa: 'E. WILLIAM DOTY FINE ARTS BUILDING',
    dff: 'UFCU DISCH-FALK FIELD',
    eas: 'EDGAR A. SMITH BUILDING',
    ecg: 'EAST CAMPUS GARAGE',
    ecj: 'ERNEST COCKRELL JR. HALL',
    eps: 'E.P. SCHOCH BUILDING',
    erc: 'FRANK C ERWIN SPECIAL EVENTS CENTER',
    eer: 'Engineering and Education Research Center',
    etc: 'ENGINEERING TEACHING CENTER II',
    fac: 'PETER T. FLAWN ACADEMIC CENTER',
    gar: 'GARRISON HALL',
    gdc: 'GATES DELL COMPLEX',
    gea: 'MARY E. GEARING HALL',
    geb: 'DOROTHY L. GEBAUER BUILDING',
    gol: 'GOLDSMITH HALL',
    gre: 'GREGORY GYMNASIUM',
    grg: 'GEOGRAPHY BUILDING',
    gsb: 'GRADUATE SCHOOL OF BUSINESS BLDG.',
    hdb: 'HEALTH DISCOVERY BUILDING',
    hgc: 'NULL',
    hlb: 'HEALTH LEARNING BUILDING',
    hma: 'HOGG MEMORIAL AUDITORIUM',
    hrc: 'HARRY RANSOM CENTER',
    hrh: 'RAINEY HALL',
    hsm: 'WILLIAM RANDOLPH HEARST BLDG',
    htb: 'HEALTH TRANSFORMATION BUILDING',
    jcd: 'JESTER DORMITORY',
    jes: 'BEAUFORD H. JESTER CENTER',
    jgb: 'JACKSON GEOLOGICAL SCIENCES BLDG.',
    jhh: 'JOHN W. HARGIS HALL',
    jon: 'JESSE H. JONES HALL',
    kin: 'KINSOLVING DORMITORY',
    lbj: 'LYNDON B JOHNSON LIBRARY',
    ldh: 'LONGHORN DINING FACILITY',
    lfh: 'LITTLEFIELD HOME',
    lla: 'LIVING LEARNING HALL A',
    llb: 'LIVING LEARNING HALL B',
    llc: 'LIVING LEARNING HALL C',
    lld: 'LIVING LEARNING HALL D',
    lle: 'LIVING LEARNING HALL E',
    llf: 'LIVING LEARNING HALL F',
    ltd: 'LITTLEFIELD DORMITORY',
    lth: 'LABORATORY THEATER BLDG.',
    mag: 'MANOR GARAGE',
    mai: 'MAIN BUILDING',
    mbb: 'MOFFETT MOLECULAR BIOLOGY BLDG.',
    mez: 'MEZES HALL',
    mfh: 'RICHARD MITHOFF TRK/SCR  FIELDHOUSE',
    mhd: 'MOORE-HILL DORMITORY',
    mms: 'MIKE A.MYERS TRACK & SOCCER STADIUM',
    mnc: 'MONCRIEF-NEUHAUS ATHLETIC CENTER',
    mrh: 'MUSIC BUILDING & RECITAL HALL',
    mbe: 'MUSIC BUILDING & RECITAL HALL',
    msb: 'MAIL SERVICE BUILDING',
    nez: 'NORTH END ZONE BUILDING',
    nhb: 'NORMAN HACKERMAN BUILDING',
    nms: 'NEURAL AND MOLECULAR SCIENCE BLDG.',
    noa: 'NORTH OFFICE BUILDING A',
    nst: 'NST',
    nur: 'NURSING SCHOOL',
    pac: 'PERFORMING ARTS CENTER',
    pai: 'T.S. PAINTER HALL',
    par: 'PARLIN HALL',
    pat: 'J.T. PATTERSON LABS.BLDG.',
    pcl: 'PERRY-CASTANEDA LIBRARY',
    phd: 'PRATHER HALL DORMITORY',
    phr: 'PHARMACY BUILDING',
    pob: 'PETER ODONNELL JR. BUILDING',
    ppa: 'HAL C. WEAVER POWER PLANT ANNEX',
    ppb: 'PRINTING AND PRESS BLDG.',
    ppe: 'HAL C WEAVER POWER PLANT EXPANSION',
    ppl: 'HAL C. WEAVER POWER PLANT',
    rhd: 'ROBERTS HALL DORMITORY',
    rlm: 'ROBERT LEE MOORE HALL',
    rsc: 'RECREATIONAL SPORTS CENTER',
    rrh: 'ROBERT B ROWLING HALL',
    sac: 'STUDENT ACTIVITY CENTER',
    sea: 'SARAH M. & CHARLES E. SEAY BUILDING',
    ser: 'SERVICE BUILDING',
    sjg: 'SAN JACINTO GARAGE',
    sjh: 'SAN JACINTO RESIDENCE HALL',
    smc: 'SETON MEDICAL CENTER',
    sof: 'TELECOMM.SVC.SATELLITE OPS FACILITY',
    srh: 'SID RICHARDSON HALL',
    ssb: 'STUDENT SERVICES BUILDING',
    ssw: 'SCHOOL OF SOCIAL WORK BUILDING',
    std: 'DARRELL K ROYAL TX MEMORIAL STADIUM',
    sut: 'SUTTON HALL',
    sw7: 'SPEEDWAY OFFICE BUILDING',
    swg: 'SPEEDWAY GARAGE',
    szb: 'GEORGE I. SANCHEZ BUILDING',
    tcc: 'JOE C THOMPSON CONFERENCE CENTER',
    tes1: 'THERMAL ENERGY STORAGE 1',
    tes2: 'THERMAL EXPANSION TANK 2',
    tmm: 'TEXAS MEMORIAL MUSEUM',
    tnh: 'TOWNES HALL',
    trg: 'TRINITY GARAGE',
    tsc: 'LEE & JOE JAMAIL TEXAS SWIMMING CTR',
    tsg: '27TH STREET GARAGE',
    ttc: 'TEXAS TENNIS CENTER',
    ua9: '2609 UNIVERSITY AVENUE',
    uil: 'UNIV. INTERSCHOLASTIC LEAGUE BLDG.',
    unb: 'UNION BUILDING',
    upb: 'UNIVERSITY POLICE BUILDING',
    uta: 'UT ADMINISTRATION BUILDING',
    utc: 'UNIVERSITY TEACHING CENTER',
    utx: 'ETTER-HARBIN ALUMNI CENTER',
    wag: 'WAGGENER HALL',
    wch: 'WILL C. HOGG BLDG.',
    wel: 'ROBERT A. WELCH HALL',
    win: 'F.L. WINSHIP DRAMA BLDG.',
    wmb: 'WEST MALL OFFICE BLDG.',
    glt: 'GARY L THOMAS',
    wwh: 'WALTER WEBB HALL' 
};
var circuits = {
    'com': ['5N8', '5N5'], 
    'lth': ['WN2', 'WS5'], 
	'mai': ['5N8', '5S1'], 
	'phd': ['5N5', '5S4'], 
	'rhd': ['5N6', '5S4'], 
	'par': ['5N5', '5S4'], 
	'pai': ['5N8', '5N5'], 
	'sut': ['5N8', '5N5'], 
	'tmm': ['5N1', '5S6'], 
	'upb': ['5N1', '5S6'], 
	'utx': ['5N5', '5S4'], 
	'uta': ['Austin Energy', ''], 
	'std': ['HE3' ,'HW3'], 
	'wag': ['5N5' ,'5S4'], 
	'rrh': ['WW14' ,'WE14'], 
	'btl': ['5N8' ,'5N5'], 
	'wmb': ['5N8' ,'5N5'], 
	'ccj': ['HS1' ,'HN1'], 
	'art': ['5N1' ,'5S6'], 
	'crh': ['5N1' ,'5S6'], 
	'bhd': ['5N6' ,'5S4'], 
	'and': ['5N8' ,'5S6'], 
	'crd': ['5N8' ,'5S6'], 
	'lfh': ['5N8' ,'5S6'], 
	'gea': ['5N8' ,'5S6'], 
	'gol': ['5N8' ,'5S6'], 
	'bat': ['5S4' ,'5N5'], 
	'ben': ['5S4' ,'5N5'], 
	'cal': ['5S4' ,'5N5'], 
	'gar': ['5S4' ,'5N5'], 
	'bld': ['5S6' ,'5N1'], 
	'bur': ['5S6' ,'5N1'], 
	'gwb': ['5S6' ,'5N1'], 
	'ccs#5': ['AE/W-5' ,''], 
	'ccs#6': ['AE/W-6' ,''], 
	'adh': ['WE3' ,'WW3'], 
	'anb': ['WE10' ,'WW10'], 
	'arc': ['WE3' ,'WW3'], 
	'att': ['WW14' ,'WE14'], 
	'bel': ['WW5' ,'WE5'], 
	'bio': ['AW2' ,'AE2'], 
	'ccs#4': ['HE/W-5' ,''], 
	'bma': ['WW2' ,'WE2'], 
	'bmc': ['WW3' ,'WE3'], 
	'bme': ['AW2' ,'AE2'], 
	'brb': ['WE15' ,'WW15'], 
	'ltd': ['5S6' ,'5S6'], 
	'brg': ['WW2' ,'WE2'], 
	'cba': ['WN4' ,'WS4'], 
	'mez': ['5N5' ,'5S4'], 
	'ccj': ['HS1' ,'HN1'], 
	'cdl': ['WW10' ,'WE10'], 
	'cee': ['WE3' ,'WW3'], 
	'cla': ['WW15' ,'WE15'], 
	'cma': ['WW3' ,'WE3'], 
    'cmb': ['WW3' ,'WE3'], 
	'cpe': ['AW5' ,'AE5'], 
	'crb': ['HS3' ,'HN3'], 
	'ccs#7': ['HS/N-2' ,''], 
	'das': ['HE3' ,'HW3'], 
	'ccs#3': ['WE/W-12' ,''], 
	'dfa': ['WE7' ,'WW7'], 
	'dcp': ['WE10' ,'WW10'], 
	'dff': ['HE3' ,'HW3'], 
	'eas': ['WW2' ,'WE2'], 
	'ecg': ['HE3' ,'HW3'], 
	'ecj': ['WN5' ,'WS3'], 
	'ecs': ['HE3' ,'HW3'], 
	'eer': ['HE4' ,'HW4'], 
	'eps': ['WE15' ,'WW15'], 
	'bwy': ['WE6' ,'WW6'], 
	'erc': ['WE10' ,'WW10'], 
	'etc': ['AE5' ,'AW5'], 
	'fac': ['AE2' ,'AW2'], 
	'cba': ['WN4' ,'WS4'], 
	'geb': ['WN4' ,'WS4'], 
	'ecj': ['WN5' ,'WS3'], 
	'gre': ['WS4' ,'WN4'], 
	'mhd': ['WS4' ,'WN4'], 
	'fc1': ['HE3' ,'HW3'], 
	'fc2': ['HE3' ,'HW3'], 
	'glt': ['WN5', 'WS3'], 
	'fc3': ['HE3' ,'HW3'], 
	'fc4': ['HE3' ,'HW3'], 
	'fc5': ['HE3' ,'HW3'], 
	'fc6': ['HE3' ,'HW3'], 
	'fc7': ['HE3' ,'HW3'], 
	'fc8': ['HE3' ,'HW3'], 
	'fc9': ['HE3' ,'HW3'], 
	'fnt': ['AW9' ,'AE9'], 
	'gdc': ['WW15' ,'WE15'], 
	'geb': ['WN4' ,'WS4'], 
	'gsb': ['WE14' ,'WW14'], 
	'hcg': ['WN2' ,'WS5'], 
	'hdb': ['WN2' ,'WS5'], 
	'hlb': ['WS5' ,'WN2'], 
	'hrc': ['WE14' ,'WW14'], 
	'hsm': ['WW3' ,'WE3'], 
	'htb': ['WN2' ,'WS5'], 
	'jes': ['WW14' ,'WE14'],
	'ldh': ['WW14' ,'WE14'], 
	'jgb': ['WS3' ,'WN5'], 
	'hrh': ['5N5' ,'5S4'], 
	'jhh': ['WE10' ,'WW10'], 
	'jon': ['HS1' ,'HN1'], 
	'jum': ['HE3' ,'HW3'], 
	'kin': ['WE3' ,'WW3'], 
	'lbj': ['WW7' ,'WE7'], 
	'mag': ['HE3' ,'HW3'], 
	'mbb': ['AW9' ,'AE9'], 
	'mbe': ['WE5' ,'WW5'], 
	'mnc': ['HW3' ,'HE3'], 
	'mrh': ['WE5' ,'WW5'], 
	'msb': ['HN3' ,'HS3'], 
	'nez': ['WE5' ,'WW5'], 
	'nhb': ['AW4' ,'AE4'], 
	'nms': ['AW5' ,'AE5'], 
	'noa': ['WE8' ,'WW8'], 
	'nur': ['WE10' ,'WW10'], 
	'pac': ['WE7' ,'WW7'], 
	'pat': ['WN5' ,'WS3'], 
	'pcl': ['WE2' ,'WW2'], 
	'phr': ['AW9' ,'AE9'], 
	'ahg': ['AW9' ,'AE9'], 
	'pob': ['AW2' ,'AE2'], 
	'rbrh': ['WE14' ,'WW14'], 
	'rlm': ['WE6' ,'WW6'], 
	'rsc': ['WE10' ,'WW10'], 
	'sac': ['WE15' ,'WW15'], 
	'sea': ['WE8' ,'WW8'], 
	'ser': ['WW8' ,'WE8'], 
	'sjg': ['HS1' ,'HN1'], 
	'sjh': ['WW2' ,'WE5'], 
	'sof': ['HE3' ,'HW3'], 
	'srh': ['WW7' ,'WE7'], 
	'ssb': ['AE9' ,'AW9'], 
	'ssw': ['WW10' ,'WE10'], 
	'stde': ['HW3' ,'HE3'], 
	'sw7': ['WE3' ,'WW3'], 
	'swg': ['WE8' ,'WW8'], 
	'szb': ['WW2' ,'WE2'], 
	'tay': ['AW2' ,'AE2'], 
	'tcc': ['WE5' ,'WW5'], 
	'tnh': ['HS1' ,'HN1'], 
	'trg': ['WW10' ,'WE10'], 
	'tsc': ['WW10' ,'WE10'], 
	'tsg': ['WE8' ,'WW8'], 
	'ttc': ['HE3' ,'HW3'], 
	'ua9': ['AE9' ,'AW9'], 
	'uil': ['HE3' ,'HW3'], 
	'unb': ['AE2' ,'AW2'], 
	'utc': ['WE2' ,'WW2'], 
	'wch': ['WN4' ,'WS4'], 
	'wel': ['WN4' ,'WS4'], 
	'wel': ['WW16' ,'WE16'], 
	'win': ['WE16' ,'WW16'], 
	'wwh': ['WW3' ,'WE3']
}
function add_circuit(tag, circuits){
    tag["circuit"] = circuits[tag["who"].split("/")[0].toLowerCase()]; 
    if(typeof tag.circuit === 'undefined'){
        tag.circuit = '';
        console.log(tag.who);
    }
    return tag;
}
function add_circuits(tags){
    var alarms_with_circuits = []; 
    tags.forEach(function(tag){
        alarms_with_circuit = add_circuit(tag, circuits); 
        alarms_with_circuits.push(alarms_with_circuit); 
    }); 
    return alarms_with_circuits   
}
exports.alarms_acknowledge = function(data, callback) { 
    var body = "";
    var post_data = querystring.stringify(data);
    var post_options = {
      host: ignition_machine,
      port: ignition_port,
      path: ignition_alarms_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
            callback(JSON.stringify(body)); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};
exports.alarms_ignore = function(data, callback) { 
    var body = "";
    var post_data = querystring.stringify(data);
    var post_options = {
      host: ignition_machine,
      port: ignition_port,
      path: ignition_ignore_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
        	// console.log(body);
            callback(body); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};
exports.alarms_shelve = function(received_from_operator, callback) { 
    var body = "";
    var post_data = querystring.stringify(received_from_operator);
    var post_options = {
      host: ignition_machine,
      port: ignition_port,
      path: ignition_shelve_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
            callback(body); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};
exports.alarms_escalate = function(data, callback) { 
    var body = "";
    var post_data = querystring.stringify(data);
    var post_options = {
      host: ignition_machine,
      port: ignition_port,
      path: ignition_escalate_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
        	// console.log(body);
            callback(body); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};
exports.alarms_deescalate = function(data, callback) { 
    var body = "";
    var post_data = querystring.stringify(data);
    var post_options = {
      host: ignition_machine,
      port: ignition_port,
      path: ignition_deescalate_path,
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data),
          'Access-Control-Allow-Origin': '*'
      }
    };
    var post_req = http.request(post_options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on("end", () => {
        	// console.log(body);
            callback(body); 
        });
    });
    post_req.write(post_data);
    post_req.end();
};

exports.alarms_for_management_table = function(callback) {
    var body = "";

    const options = {
      hostname: ignition_machine,
      port: ignition_port,
      path: ignition_alarms_path,
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    };

    const req = http.request(options, function(response) {
        response.setEncoding("utf8");
        response.on("data", data => {
            body += data;
        });
        response.on("end", () => {
            try {
                var data = JSON.parse(body);
                var kepware_opc_server_state = data.kepware_opc_server_state;
                var shelved_tags = normalize_shelved_tags(data.shelved_tags);
                var tags;
                tags = normalize_alarms_from_events(data.rows);
                tags = get_latest_alarms(tags);
                tags = filter_for_tag_building_alarm_when_a_slash(tags);
                tags = exports.filter_for_operator(tags);
                tags = should_display(tags);
                tags = should_siren(tags);
                tags = is_shelvable(tags);
                tags = filter_for_shelved_alarms(shelved_tags, tags);
                tags = presentation_logic(tags);
                alarms = {}
                alarms.tags = tags;
                alarms.ia_opc_server_state = "CONNECTED";
                alarms.kepware_opc_server_state = kepware_opc_server_state;
                debug("------------------------------: " + tags.length);
                callback(null, alarms); 
            } catch(err) {
                console.log('callback try/catch: ' + err);
                alarms = {}
                alarms.tags = [];
                alarms.ia_opc_server_state = "CONNECTED";
                alarms.kepware_opc_server_state = "FAULTED";
                callback(null, alarms); 
            }
        });
    });
    req.on('error', (err) => {
        console.log('request error: ' + err);
        alarms = {}
        alarms.tags = [];
        alarms.ia_opc_server_state = "FAULTED";
        alarms.kepware_opc_server_state = "UNKNOWN";
        callback(null, alarms); 
    });  
    req.end();

    function normalize_shelved_tags(shelved_tags){
        for(shelved_tag of shelved_tags){
            shelved_tag.expired = JSON.parse(shelved_tag.expired.toLowerCase());
        }
        return shelved_tags;
    }
    function filter_for_shelved_alarms(shelved_tags, tags){
        var filtered_tags = [];
        if(shelved_tags.length == 0){
            filtered_tags = tags;
        } else {
            for(tag of tags){
                var building = tag.who.split('/')[0];
                var is_shelved = false;
                for(shelved_tag of shelved_tags){
                    var shelved_building = shelved_tag.who.split('/')[1];
                        is_shelved = is_shelved || (building == shelved_building);
                }   
                if(!is_shelved){
                    filtered_tags.push(tag);
                }
            }
        }
        debug("filter_for_shelved_alarms: " + filtered_tags.length);
        return filtered_tags;
    }
    function is_shelvable(tags){
        for(tag of tags){
            tag.is_shelvable = !tag.escalated;
        }
        debug("is_shelvable: " + tags.length);
        return tags;
    }
    function should_display(tags){
        var filtered_tags = [];
        for(tag of tags){
            var alarming = tag.alarming;
            var escalated = tag.escalated;
            tag.should_display = alarming || escalated;
            filtered_tags.push(tag);
        }
        debug("should_display: " + filtered_tags.length);
        return filtered_tags;
    }
    function presentation_logic(tags){
        var tags_formatted = [];
        for(tag of tags){
            var pieces = tag.who.split('/');
            var building = pieces[0];
            var name = pieces[1];
            var constraint = pieces[2];
            var who = building.toUpperCase()+'/'+name+'/'+constraint;
            tag.who = who;
            tags_formatted.push(tag);
        }
        debug("presentation_logic: " + tags_formatted.length);
        return tags_formatted;
    }
};
function get_latest_alarms(tags) {
    tags.sort(function(a, b){return a.when_alarmed - b.when_alarmed});
    var alarms_latest_who = [];
    var alarms_latest_alarms = [];
    tags.forEach(function(tag){
        var found_at = alarms_latest_who.indexOf(tag.who);
        if(found_at != -1){
            alarms_latest_alarms[found_at] = tag;
        } else {
            alarms_latest_who.push(tag.who);
            alarms_latest_alarms.push(tag);
        }
    })
    debug("get_latest_alarms: " + alarms_latest_alarms.length);
    return alarms_latest_alarms; 
}
function sort_alarms_for_history(tags) {
    tags.sort(function(a, b){return b.when_alarmed - a.when_alarmed});
    debug("sort_alarms_for_history: " + tags.length);
    return tags; 
}
normalize_alarm_from_event = function(tag){
    var alarming = JSON.parse(tag[0]);
    var acknowledged = JSON.parse(tag[1]);
    var when_alarmed = Date.parse(tag[2]);
    if (tag[3] == null){
        who = '(none)'
    } else {
        var who = tag[3];
        var pieces = who.split('/');
        who = pieces[0]+'/'+pieces[1]+'/'+pieces[2];
    }
    var why_alarming = tag[4]; 
    var uuid = tag[5];
    var shelved = JSON.parse(tag[6]);
    var escalated = JSON.parse(tag[7]);
    var when_cleared = Date.parse(tag[8]);
    var ignored = JSON.parse(tag[9]);

    alarm_data_normalized = {
        "alarming":alarming, 
        "acknowledged":acknowledged, 
        "when_alarmed":when_alarmed, 
        "who":who, 
        "why_alarming":why_alarming, 
        "uuid":uuid, 
        "shelved":shelved,
        "escalated":escalated,
        "when_cleared":when_cleared,
        "ignored":ignored,
    }; 
    return alarm_data_normalized; 
}
normalize_alarms_from_events = function(tags) {
    var alarms_normalized = []; 
    for(tag of tags){
    	var who = tag[3].split('/')[0].toLowerCase();
    	var is_building = who.indexOf('_') == -1;
    	var exists = locations[who] != undefined;
    	if(is_building && exists){
	        var alarm_normalized = normalize_alarm_from_event(tag);
	        alarms_normalized.push(alarm_normalized); 
    	}
    }
    debug("normalize_alarms_from_events: " + alarms_normalized.length);
    return alarms_normalized; 
}
exports.normalize_alarm_historical = function(tag){
    var uuid = tag[5];
    var who = tag[0];
    if (who.indexOf('/') == -1){
        // who = '(none)'
    } else {
        var who = tag[0];
        var pieces_on_slash = who.split('/');
        var pieces_on_colon = pieces_on_slash[1].split(':');
        var building_part_uppercased = pieces_on_colon[0] + ':' + pieces_on_colon[1].toUpperCase();
        who = pieces_on_slash[0] + '/' + building_part_uppercased + '/' + pieces_on_slash[2] + '/' + pieces_on_slash[3];
    }
    tag[0] = who;
    var alarmed_data = tag[1];
    var alarming = alarmed_data.indexOf('eventTime') != -1 ? 1: 0;
    var acknowledged_data = tag[2];
    var acknowledged = acknowledged_data.indexOf('eventTime') != -1 ? 1: 0;
    var cleared_data = tag[3]; 
    var cleared = cleared_data.indexOf('eventTime') != -1 ? 1: 0;
    var state = cleared * 4 + acknowledged * 2 + alarming; 
    var because; 
    switch(state){ 
        case 1: // alarming
            alarmed_data = alarmed_data.substring(1,alarmed_data.length-1); 
            alarmed_data = alarmed_data.split(', ');
            var aHash = {};
            for(elementN in alarmed_data){
                var pieces = alarmed_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            because = 'It became ' + aHash['eventValue'];
            break;
        case 2: 
        case 3: // Acknowledged
            acknowledged_data = acknowledged_data.substring(1,acknowledged_data.length-1);
            acknowledged_data = acknowledged_data.split(', ');
            var aHash = {};
            for(elementN in acknowledged_data){
                var pieces = acknowledged_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            user = aHash['ackUser']; 
            if(user != undefined) 
                var user = user.split(':')[1]; 
            if(user == 'Live Event Limit'){
                var reason = "it was automatically acknowledged"
            } else {
                var reason = aHash['ackNotes']
            }
            because = user + ' SAYS ' + reason;
            break;
        case 4: 
        case 5: 
        case 6: 
        case 7: // Cleared
            cleared_data = cleared_data.substring(1,cleared_data.length-1);
            cleared_data = cleared_data.split(', ');
            var aHash = {};
            for(elementN in cleared_data){
                var pieces = cleared_data[elementN].split('=');
                var lpiece = pieces[0];
                var rpiece = pieces[1];
                aHash[lpiece] = rpiece;
            }
            timeAck = aHash['eventTime'];
            because = 'It became ' + aHash['eventValue'];
            break; 
        default:
            timeAck = '(unknown)';
    }; 
    switch(state){
        case 1:
            var became = 'Active'; 
            break; 
        case 2:
            var became = 'Acknowledged'; 
            break; 
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            var became = 'Cleared'; 
            break; 
        default:
            var became = '(unknown)'; 
    } 
    alarm_data_normalized = {
        "when_alarmed":Date.parse(timeAck), 
        "who":who, 
        "alarmed_data":alarmed_data, 
        "acknowledged_data":acknowledged_data, 
        "cleared_data":cleared_data, 
        "became":became, 
        "because": because, 
        "uuid": uuid, 
    };

    return alarm_data_normalized; 
}

exports.normalize_alarms_historical = function(tags) {
    var tags_normalized = []; 
    for(tag of tags){
        var tag_normalized = exports.normalize_alarm_historical(tag);
        tags_normalized.push(tag_normalized); 
    }
    debug("normalize_alarms_historical: " + tags_normalized.length);
    return tags_normalized; 
}

function filter_out_infrastructure_tags(tags){
    var alarms_filtered = []; 
    for(tag of tags){
        var who = tag.who.split('/')[0];
        if(who == "ARC") continue;
        if(who == "CRB") continue;
        if(who == "COM") continue;
        if(who == "MTC") continue;
        alarms_filtered.push(tag);
    }
    debug("filter_for_escalate_tags: " + alarms_filtered.length);
    return alarms_filtered;
}

function filter_for_escalate_tags(tags){
    var alarms_filtered = []; 
    for(tag of tags){
        var tag_name = tag.who.split('/')[1];
        var escalate_tag = tag_name.toLowerCase() == 'escalate';
        if(escalate_tag)
            alarms_filtered.push(tag);
    }
    debug("filter_for_escalate_tags: " + alarms_filtered.length);
    return alarms_filtered;
}

function filter_for_operator_actions_or_sys_events(tags){
    var tags_filtered = []; 
    for(tag of tags){
        if(tag.who.indexOf('/') == -1){
            tags_filtered.push(tag);
        } else {
            var tag_path = tag.who.split(':')[3];
            var tag_name = tag_path.split('/')[1];
            var building_tag = tag_name.toLowerCase() != 'syserr';
            if(building_tag)
                tags_filtered.push(tag);
        }
    };
    return tags_filtered;
}

function filter_for_tag_building_alarm_when_a_slash(tags){
    var tags_filtered = []; 
    for(tag of tags){
        var tag_name = tag.who.split('/')[1];
        var building_tag = tag_name.toLowerCase() == 'BldgOut'.toLowerCase();
        if(building_tag){
            // console.log("keep: " + tag_name);
            tags_filtered.push(tag);
        } else {
            // console.log("discard: " + tag_name);
        }
    };
    debug("filter_for_tag_building_alarm_when_a_slash: " + tags_filtered.length);
    // debugger;
    return tags_filtered;
}

exports.filter_for_operator = function(tags) {
    var alarms_filtered = []; 
    for(tag of tags){
        var alarming = tag["alarming"]
        var shelved = tag["shelved"]
        var escalated = tag["escalated"]
        if((alarming || escalated) && !shelved){
            alarms_filtered.push(tag); 
        }
    }
    debug("filter_for_operator: " + alarms_filtered.length);
    return alarms_filtered;
}
function should_siren(tags){
    for(tag of tags){
        var alarming = tag.alarming;
        var acknowledged = tag.acknowledged;
        var escalated = tag.escalated;
        tag.should_siren = (!alarming && escalated) || (alarming && !acknowledged);
        if(alarming && !acknowledged && escalated){
        	tag.should_siren = false;
        }
    }
    debug("should_siren: " + tags.length);
    return tags;
}
function filter_for_public_table(tags) {
    var tags_filtered = [];
    for(tag of tags){
        var escalated = tag.escalated;
        var recently_changed = tag.recently_changed;
        if(escalated || recently_changed){
            tags_filtered.push(tag); 
        }
    }
    debug("normalize_alarms_from_events: " + tags_filtered.length);
    return tags_filtered; 
}

function add_age(tags){
    var current_time = Date.now();
    tags.forEach(function(tag){
        if(tag["alarming"]){
            var elapsed_time = Date.now() - tag.when_alarmed;
        } else {
            var elapsed_time = Date.now() - tag.when_cleared;
        }
        tag.age = elapsed_time;
    });
    debug("add_age: " + tags.length);
    return tags; 
}

function is_alarm_recently_changed(tag) {
    var elapsed_time;
    if(tag["alarming"]){
        elapsed_time = Date.now() - tag["when_alarmed"];
    } else {
        elapsed_time = Date.now() - tag["when_cleared"];
    }
    var recently_changed = elapsed_time < 10 * 60 * 1000;
    return recently_changed;
}

function is_alarm_state_changed_recently(tags){
    for(ndx in tags){
        tags[ndx].recently_changed = is_alarm_recently_changed(tags[ndx]);
    }
    debug("is_alarm_state_changed_recently: " + tags.length);
    return tags;
}

exports.alarms_for_public_table = function(callback) { 
    var url = ignition_alarms_uri; 
    var body = "";
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
      	try {
	        var data = JSON.parse(body);
            var tags;
            tags = normalize_alarms_from_events(data.rows);
	        tags = get_latest_alarms(tags);
	        tags = filter_for_escalate_tags(tags);
	        tags = is_alarm_state_changed_recently(tags);
	        tags = filter_for_public_table(tags);
	        tags = add_age(tags);
	        tags = add_alarm_count(tags);
	        tags = presentation_logic(tags);
	        callback(tags);
      	} catch (err){
            console.log('exception: alarms_for_public_table: trial expired?');
            callback([]); 
        }
      });
    });
    function add_alarm_count(tags){
        var alarming_count = 0;
        for(i=0; i < tags.length; i++){
            if(tags[i].alarming)
                alarming_count = alarming_count + 1;
        }
        var alarms_data = {
            alarming_count: alarming_count,
            tags: tags
        }
        debug("add_alarm_count: " + alarms_data.length);
        return alarms_data;
    }
	function presentation_logic(tags_w_count){
	    var tags = tags_w_count.tags;
	    var tags_formatted = [];
	    for(ndx in tags){
	        var pieces = tags[ndx].who.split('/');
	        var building = pieces[0];
	        var name = pieces[1];
	        var constraint = pieces[2];
	        var who = building.toUpperCase()+'/'+name+'/'+constraint;
	        tags[ndx].who = who;
	        tags_formatted.push(tags[ndx]);
	    }
	    tags_w_count.tags = tags_formatted;
        debug("presentation_logic: " + tags_w_count.length);
	    return tags_w_count;
	}
};

exports.alarms_for_public_map = function(callback) { 
    var url = ignition_alarms_uri; 
    var body = "";
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
      	// try {
	        var data = JSON.parse(body);
	        var tags = normalize_alarms_from_events(data.rows);
	        var tags = get_latest_alarms(tags);
	        tags = filter_out_infrastructure_tags(tags);
	        tags = filter_for_escalate_tags(tags);
	        tags = eliminate_duplicate_buildings(tags);
	        tags = add_locations(tags);
	        tags = presentation_logic(tags);
	        callback(tags); 
      	// } catch (err){
       //      console.log('exception: alarms_for_public_map: trial expired?');
       //      callback([]); 
       //  }
      });
    });
    function presentation_logic(tags){
        var tags_formatted = [];
        for(ndx in tags){
            var pieces = tags[ndx].who.split('/');
            var building = pieces[0];
            var name = pieces[1];
            var constraint = pieces[2];
            var who = building.toUpperCase()+'/'+name+'/'+constraint;
            tags[ndx].who = who;
            tags_formatted.push(tags[ndx]);
        }
        return tags_formatted;
    }
    function eliminate_duplicate_buildings(tags){
        var buildings = [];
        var unique_alarms = [];
        for(i=0; i!=tags.length; i++){
            if(buildings.indexOf(tags[i].who) == -1){
                buildings.push(tags[i].who);
                unique_alarms.push(tags[i]);
            }
        }
        return unique_alarms;
    };
};
exports.alarms_for_management_map = function(callback) { 
    var url = ignition_alarms_uri; 
    var body = "";
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
      	try {
	        var data = JSON.parse(body);
	        var tags = normalize_alarms_from_events(data.rows);
	        tags = get_latest_alarms(tags);
	        tags = filter_for_tag_building_alarm_when_a_slash(tags);
	        tags = eliminate_duplicate_buildings(tags);
            tags = add_locations(tags);
            tags = add_full_names(tags);
            tags = add_circuits(tags);
	        callback(tags); 
      	} catch (err){
            console.log('exception: alarm_sfor_management_map: trial expired?');
            callback([]); 
        }
      });
    });
    function eliminate_duplicate_buildings(tags){
        var buildings = [];
        var unique_alarms = [];
        for(i=0; i!=tags.length; i++){
            if(buildings.indexOf(tags[i].who) == -1){
                buildings.push(tags[i].who);
                unique_alarms.push(tags[i]);
            }
        }
        return unique_alarms;
    };
};

function add_location(tag, locations){
    tag["where"] = locations[tag["who"].split("/")[0].toLowerCase()]; 
    return tag;    
}

function add_locations(tags){
    var alarms_with_locations = []; 
    tags.forEach(function(tag){
        alarm_with_location = add_location(tag, locations); 
        alarms_with_locations.push(alarm_with_location); 
    }); 
    return alarms_with_locations   
}

function add_full_name(tag){
    tag.full_name = full_names[tag.who.split('/')[0].toLowerCase()];
    return tag;    
}

function add_full_names(tags){
    var tags_out = []; 
    for(tag of tags){
        tag = add_full_name(tag);
        tags_out.push(tag); 
    }; 
    return tags_out   
}

function filter_for_testing(data){
    var rows = data.rows;
    var columns = data.columns;
    var rows_filtered = []; 
    for(row of rows){
        var uuid = row[5];
        if(uuid == "24928f55-4be7-44b3-85b3-b34d4a944cde"){
            rows_filtered.push(row);
        }
    }
    return {rows:rows_filtered, cols:columns};
}

exports.alarms_for_history = function(callback) { 
    var url = ignition_history_uri; 
    var body = "";
    http.get(url, res => {
      res.setEncoding("utf8");
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        // try {
            var data = JSON.parse(body);
            // debugger;
            var tags = data.rows;
            debug("history tag count normalize_alarms_historical: " + tags.length);
            tags = exports.normalize_alarms_historical(tags);
        	// debugger;
            debug("history tag count filter_for_building_in_alarm: " + tags.length);
            // debugger;
            tags = filter_for_building_in_alarm(tags);
        	// debugger;
            debug("history tag count sort_alarms_for_history: " + tags.length);
            tags = sort_alarms_for_history(tags);
        	// debugger;
            debug("history tag count presentation_logic: " + tags.length);
            tags = presentation_logic(tags);
        	// debugger;
            debug("history tag count callback: " + tags.length);
            callback(tags);
        // } catch (err){
        // 	debugger;
        //     console.log('exception: alarms_for_history: trial expired?');
        //     callback([]); 
        // }
      });
    });
    function filter_for_building_in_alarm(tags){
        var tags_filtered = []; 
        for(tag of tags){
            if(tag.who.indexOf('/') == -1){
                tags_filtered.push(tag);
            } else {
                var tag_path = tag.who.split(':')[3];
                var tag_name = tag_path.split('/')[1];
                var building_tag = tag_name.toLowerCase() != 'syserr'.toLowerCase();
                if(building_tag)
                    tags_filtered.push(tag);
            }
        };
        // debugger;
    	debug("filter_for_building_in_alarm: " + tags_filtered.length);
        return tags_filtered;
    }
    function presentation_logic(tags){
        var tags_formatted = [];
        for(tag of tags){
            if(tag.who.indexOf('/') == -1){
                tags_formatted.push(tag);
            } else {
                var pieces = tag.who.split(':');
                var building = pieces[3].split('/')[0];
                var building_tag = pieces[3].split('/')[1];
                tag.who = building.toUpperCase()+'-'+building_tag;
                tags_formatted.push(tag);
            }
        }
    	debug("history presentation_logic: " + tags_formatted.length);
        return tags_formatted;
    }
};

exports.acknowledge = function(uuid,user,notes){
    var body = "";

    const postData = uuid;

    const options = {
      hostname: ignition_machine,
      port: ignition_port,
      path: ignition_alarms_path+"?uuid="+uuid+"&user="+user+"&notes="+notes,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        // 'Content-Length': Buffer.byteLength(postData)
      }
    };

    const req = http.request(options, function(res) {
        res.setEncoding('utf8');
        res.on("data", data => {
            body += data;
        });
        res.on('end', () => {
        // response from ignition ended
        });
    });
    req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`);
    });
    req.end();
    return 'ok';
}