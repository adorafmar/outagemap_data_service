const debug = require('debug')('tags')
var fs = require('fs')
var express = require('express');
var app = express(); 
var alarms = require("./alarms");
const querystring = require("querystring");
var https = require('https') 

app.set('port', 8080);

app.get('/', function (req, res) {
	res.header('Access-Control-Allow-Origin' , '*');
	res.send('Server is on line')
})

app.get('/alarms_for_management_table', function (req, res) {
	function callback(err, data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	// debugger;
	alarms.alarms_for_management_table(callback); 
})

app.get('/alarms_for_public_table', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_public_table(callback); 
})

app.get('/alarms_for_management_map', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_management_map(callback); 
})

app.get('/alarms_for_public_map', function (req, res) {
	function callback(data){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_public_map(callback); 
})

app.get('/alarms_for_history', function (req, res) {
	function callback(data){
		debug("history callback to server tag count: " + data.length);
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
	alarms.alarms_for_history(callback); 
})

app.post('/acknowledge', function (req, res) {
	  var body = '';
	  var received_from_operator;
      req.on("data", data => {
        body += data;
      });
      req.on("end", () => {
      	received_from_operator = querystring.parse(body)
		alarms.alarms_acknowledge(received_from_operator, callback); 
      });
	function callback(data){
		console.log(data);
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(data); 
		res.send(body); 
	}
})

app.post('/ignore', function (req, res) {
	var posted_data = '';
	var received_from_operator;
	function callback(received_from_ignition){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(received_from_ignition); 
		res.send(body); 
	}
	req.on("data", chunk => {
		posted_data += chunk;
	});
	req.on("end", () => {
		received_from_operator = querystring.parse(posted_data);
		alarms.alarms_ignore(received_from_operator, callback);
	});
})

app.post('/shelve', function (req, res) {
	var posted_data = '';
	var received_from_operator;
	function callback(received_from_ignition){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(received_from_ignition); 
		res.send(body); 
	}
	req.on("data", chunk => {
		posted_data += chunk;
	});
	req.on("end", () => {
		received_from_operator = querystring.parse(posted_data);
		alarms.alarms_shelve(received_from_operator, callback);
	});
})

app.post('/escalate', function (req, res) {
	var chunks_from_operator = '';
	var received_from_operator;
	function callback(received_from_ignition){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(received_from_ignition); 
		res.send(body); 
	}
	req.on("data", chunk => {
		chunks_from_operator += chunk;
	});
	req.on("end", () => {
		received_from_operator = querystring.parse(chunks_from_operator);
		alarms.alarms_escalate(received_from_operator, callback);
	});
})

app.post('/deescalate', function (req, res) {
	var chunks_from_operator = '';
	var received_from_operator;
	function callback(received_from_ignition){
		res.header('Access-Control-Allow-Origin' , '*');
		var body = JSON.stringify(received_from_ignition); 
		res.send(body); 
	}
	req.on("data", chunk => {
		chunks_from_operator += chunk;
	});
	req.on("end", () => {
		received_from_operator = querystring.parse(chunks_from_operator);
		alarms.alarms_deescalate(received_from_operator, callback);
	});
})

var server = app.listen(app.get('port'), function() {
  var port = server.address().port;
  console.log('http happens on port ' + port);
});

var ports = 4443
https.createServer({
  key:  fs.readFileSync('id_rsa'),
  cert: fs.readFileSync('outagemap_utilities_utexas_edu_cert.cer')
}, app)
.listen(ports, function () {
  console.log('https happens on %d', ports)
})