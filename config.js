var config = {
  development: {
    ignition_machine: '10.101.206.107'
  },
  default: {
    ignition_machine: '10.101.206.9'
  }
}
exports.get = function get(env) {
  return config[env] || config.default;
}