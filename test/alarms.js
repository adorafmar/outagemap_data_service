var expect = require("chai").expect;
var alarms = require("../app/alarms");
var PassThrough = require('stream').PassThrough;
var sinon = require('sinon');
var http = require('http');

function IGNITION_TAG(options) {
    var tag = [];
    tag.push(options['alarming']     || 'false'); // machine
    tag.push(options['acknowledged'] || 'false'); // operator
    tag.push(options['when_alarmed'] || "Mon Feb 26 05:36:02 CST 2018"); // machine
    tag.push(options['who']          || "GEA/bldgout/Alarm"); // machine
    tag.push(options['why_alarming'] || "{mode=Not Equal,eventValue=true,activePipeline=SysErr/SysErrActive,name=Alarm,eventTime=Mon Feb 26 05:36:02 CST 2018}"); // machine
    tag.push(options['uuid']         || "cb55c19f-2f7f-4848-ac11-a011210558e7"); // machine
    tag.push(options['shelved']      || 'false'); // operator
    tag.push(options['escalated']    || 'false'); // operator
    tag.push(options['when_cleared'] || "Mon Feb 26 06:36:02 CST 2018"); // machine
    tag.push(options['ignored']      || 'false'); // operator
    return tag;
}

function HISTORY_EVENT(options){
    var columns = [ 
        'getSource',
        'getActiveData',
        'getAckData',
        'getClearedData',
        'getNotes',
        'getId' 
    ];
    var row = [];
    row.push(options['getSource'] || 'prov:default:/tag:GDC/SysErr:/alm:Alarm'); // machine
    row.push(options['getActiveData'] || 'None'); // machine
    row.push(options['getAckData'] || 'None'); // machine
    row.push(options['getClearedData'] || 'None'); // machine
    row.push(options['getNotes'] || 'notes go here'); // machine
    row.push(options['getId'] || 'cc22ff84-142f-4d87-8bae-e988206a100d'); // machine

    return {columns: columns, rows: [row]}
}

describe("testing states", function() {
    beforeEach(function() {
        this.request = sinon.stub(http, 'request');
    });
    afterEach(function() {
        http.request.restore();
    });
    it("0a", function(done) {
        var tags = [];
        var options = {};
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("0b", function(done) {
        var tags = [];
        var options = {"alarming": true};
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            expect(datum.alarming).to.be.true;

            // var state = datum.shelved * Math.pow(2,5) + datum.shelved * Math.pow(2,4) + datum.escalated * Math.pow(2,3) + datum.ignored * Math.pow(2,2) + datum.acknowledged * Math.pow(2,1) + datum.alarming * Math.pow(2,0);
            // expect(state).to.equal(1);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("1", function(done) {
        var tags = [];
        var options = {
            "alarming": true,
            "acknowledged": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state = datum.shelved * Math.pow(2,5) + datum.shelved * Math.pow(2,4) + datum.escalated * Math.pow(2,3) + datum.ignored * Math.pow(2,2) + datum.acknowledged * Math.pow(2,1) + datum.alarming * Math.pow(2,0);
            expect(state).to.equal(3);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("3a", function(done) {
        var tags = [];
        var options = {
            "alarming": true,
            "acknowledged": true,
            "ignored": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state = datum.shelved * Math.pow(2,5) + datum.shelved * Math.pow(2,4) + datum.escalated * Math.pow(2,3) + datum.ignored * Math.pow(2,2) + datum.acknowledged * Math.pow(2,1) + datum.alarming * Math.pow(2,0);
            expect(state).to.equal(7);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("3b", function(done) {
        var tags = [];
        var options = {
            "alarming": true,
            "acknowledged": true,
            "escalated": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state = datum.shelved * Math.pow(2,5) + datum.shelved * Math.pow(2,4) + datum.escalated * Math.pow(2,3) + datum.ignored * Math.pow(2,2) + datum.acknowledged * Math.pow(2,1) + datum.alarming * Math.pow(2,0);
            expect(state).to.equal(11);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("6", function(done) {
        var tags = [];
        var options = {
            "ignored": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("7a", function(done) {
        var tags = [];
        var options = {
            "ignored": true,
            "acknowledged": true,
            "alarming": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state 
                = datum.shelved * Math.pow(2,4) 
                + datum.escalated * Math.pow(2,3) 
                + datum.ignored * Math.pow(2,2) 
                + datum.acknowledged * Math.pow(2,1) 
                + datum.alarming * Math.pow(2,0);

            expect(state).to.equal(7);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("7b", function(done) {
        var tags = [];
        var options = {
            "ignored": true,
            "acknowledged": true,
            "alarming": true,
            "shelved": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("10", function(done) {
        var tags = [];
        var options = {
            "escalated": false,
            "acknowledged": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("11a", function(done) {
        var tags = [];
        var options = {
            "escalated": true,
            "acknowledged": true,
            "alarming": false,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state 
                = datum.shelved * Math.pow(2,4) 
                + datum.escalated * Math.pow(2,3) 
                + datum.ignored * Math.pow(2,2) 
                + datum.acknowledged * Math.pow(2,1) 
                + datum.alarming * Math.pow(2,0);

            expect(state).to.equal(10);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("11b", function(done) {
        var tags = [];
        var options = {
            "escalated": true,
            "acknowledged": true,
            "alarming": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);

            var datum = tags[0];
            var state 
                = datum.shelved * Math.pow(2,4) 
                + datum.escalated * Math.pow(2,3) 
                + datum.ignored * Math.pow(2,2) 
                + datum.acknowledged * Math.pow(2,1) 
                + datum.alarming * Math.pow(2,0);

            expect(state).to.equal(11);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("23a", function(done) {
        var tags = [];
        var options = {
            "shelved": true,
            "acknowledged": true,
            "ignored": true,
            "alarming": false,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
    it("23b", function(done) {
        var tags = [];
        var options = {
            "shelved": true,
            "acknowledged": true,
            "ignored": false,
            "alarming": true,
        };
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
});

describe("alarms_for_management_table", function() {
    beforeEach(function() {
        this.request = sinon.stub(http, 'request');
    });
    afterEach(function() {
        http.request.restore();
    });

    it("should sound the siren when (alarming && !acknowledged)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'true';
        options['acknowledged'] = 'false';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            
            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);
            expect(tags[0].alarming).equal(true);
            expect(tags[0].acknowledged).equal(false);
            expect(tags[0].should_siren).equal(true);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it("should not sound the siren when (alarming && acknowledged)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'true';
        options['acknowledged'] = 'true';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            
            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);
            expect(tags[0].alarming).equal(true);
            expect(tags[0].acknowledged).equal(true);
            expect(tags[0].should_siren).equal(false);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it("should sound the siren when (!alarming && escalated)", function(done) {
        var tags = [];
        var options = {};
        options['alarming'] = 'false';
        options['escalated'] = 'true';
        var tag = IGNITION_TAG(options);
        tags.push(tag);
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            
            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);
            expect(tags[0].alarming).equal(false);
            expect(tags[0].escalated).equal(true);
            expect(tags[0].should_siren).equal(true);

            done();
        }

        alarms.alarms_for_management_table(callback); 
    });

    it("should not choke on parsing when ignition trial expires", function(done) {
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify("crap from ignition <=> }: blah blah blah} yada yada yada |}:"));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            
            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        
        alarms.alarms_for_management_table(callback); 
    });

    it("should discover only bldgout tags", function(done) {
        var tags = [];

        var options = {};
        options['alarming'] = 'true';
        options['who'] = "GEA/SysErr/Alarm";
        var tag = IGNITION_TAG(options);
        tags.push(tag);

        var options = {};
        options['alarming'] = 'true';
        options['who'] = "GEA/BldgOut/Alarm";
        var tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            
            debugger;
            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.equal(1);
            expect(tags[0].who.toLowerCase().indexOf('bldgout')).to.not.equal(-1);

            done();
        }
        alarms.alarms_for_management_table(callback);
    });

    it("should discover no alarms", function(done) {
        var time0 = Date.now();
        var options = {};
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){

            expect(data.tags).to.be.a('array');
            expect(data.tags.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });

    it("should discover one alarm", function(done) {
        var time0 = Date.now();
        var options = {};
        options['alarming'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            var tags = data.tags;
            expect(tags).to.be.a('array');
            expect(tags.length).to.not.equal(0);
            expect(tags[0]["age_magnitude"]).to.not.equal(0); 

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });

    it("given not escalated alarm should be shelvable", function(done) {
        var options = {};
        options['alarming'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            var tags = data.tags;
            var tag = tags[0];

            expect(tags.length).to.equal(1);
            expect(tag.is_shelvable).to.be.true; 

            done();
        }
        
        alarms.alarms_for_management_table(callback); 
    });

    it("given escalated alarm should not be shelvable", function(done) {
        var options = {};
        options['escalated'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, data){
            var tags = data.tags;
            var tag = tags[0];

            expect(tags.length).to.equal(1);
            expect(tag.is_shelvable).to.be.false; 

            done();
        }
        
        alarms.alarms_for_management_table(callback); 
    });

    it("doesn't crash when ignition doesn't send a building with the tag", function(done) {
        var options = {};
        options['who'] = 'blah blah blah';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        alarms.alarms_for_management_table(function(err, result) {

            expect(result.tags.length).to.equal(0);

            done();
        });
    });

    it("don't allow deescalate while alarm is true", function(done) {
        var options = {};
        options['alarming'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[], kepware_opc_server_state: 'CONNECTED'};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();

        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        alarms.alarms_for_management_table(function(err, result) {

            expect(result).to.not.be.null;
            expect(result).to.be.an('object');
            var tags = result.tags;
            expect(tags.length).to.equal(1);
            expect(tags[0].escalated).to.be.false;
            
            done();
        });
    });

    it("do not display to operator shelved alarms", function(done) {
        var options = {};
        
        options['alarming'] = 'true';
        options['shelved'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, alarm_data){

            expect(alarm_data.tags.length).to.equal(0); 

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });

    it("display to operator if escalated but not if shelved", function(done) {
        var options = {};
        
        options['escalated'] = 'true';
        options['shelved'] = 'true';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(err, alarm_data){
            expect(alarm_data.tags.length).to.equal(0); 

            done();
        }
        alarms.alarms_for_management_table(callback); 
    });
});
describe("alarms_for_management_map", function() {
    beforeEach(function() {
        this.request = sinon.stub(http, 'request');
    });
    afterEach(function() {
        http.request.restore();
    });

    it("should not choke on parsing when ignition trial expires", function(done) {
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify("crap from ignition <=> }: blah blah blah} yada yada yada |}:"));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());
        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(0);

            done();
        }
        alarms.alarms_for_management_map(callback); 
    });

    it("should discover only bldgout tags", function(done) {
        var tags = [];

        var options = {};
        options['alarming'] = 'true';
        options['who'] = "GEA/SysErr/Alarm";
        var tag = IGNITION_TAG(options);
        tags.push(tag);

        var options = {};
        options['alarming'] = 'true';
        options['who'] = "GEA/BldgOut/Alarm";
        var tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(data){
            expect(data).to.be.a('array');
            debugger;
            expect(data.length).to.equal(1);
            expect(data[0].who.toLowerCase().indexOf('bldgout')).to.not.equal(-1);

            done();
        }
        alarms.alarms_for_management_map(callback); 
    });
    it("filters out tag when ignition doesn't send a building", function(done) {
        var tags = [];

        var options = {};
        options['alarming'] = 'true';
        options['who'] = "no building in this tag";
        var tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        alarms.alarms_for_management_map(function(result) {

            expect(result.length).to.equal(0);

            done();
        });
    });
});
describe("alarms_for_history", function() {
    beforeEach(function() {
        this.request = sinon.stub(http, 'request');
    });
 
    afterEach(function() {
        http.request.restore();
    });

    it("should not choke on parsing when ignition trial expires", function(done) {
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify("crap from ignition <=> }: blah blah blah} yada yada yada |}:"));
        response_from_ignition.end();
        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());
        function callback(data){
            
            expect(data).to.be.a('array');
            expect(data.length).to.equal(0);

            done();
        }
        alarms.alarms_for_history(callback); 
    });
    it("doesn't choke when ignition doesn't send a building in the tag", function(done) {
        var options = {};
        options['getSource'] = "no building here";
        var history_events = HISTORY_EVENT(options);
        var expected_events = history_events;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_events));
        response_from_ignition.end();

        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(data){

            expect(data).to.be.an('array');

            done();
        }
        alarms.alarms_for_history(callback); 
    });
    it("syserr tags should never appear in history", function(done) {
        var options = {};
        options['getSource'] = "/SysErr";
        var history_events = HISTORY_EVENT(options);
        var expected_events = history_events;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_events));
        response_from_ignition.end();

        this.request.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(data){

            expect(data).to.be.an('array');
            expect(data.length).to.equal(0);

            done();
        }
        alarms.alarms_for_history(callback); 
    });
});
describe("alarms_for_public_table", function() {
    beforeEach(function() {
        this.get = sinon.stub(http, 'get');
    });
    afterEach(function() {
        http.get.restore();
    });

    it("should not choke on parsing when ignition trial expires", function(done) {
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify("crap from ignition <=> }: blah blah blah} yada yada yada |}:"));
        response_from_ignition.end();
        this.get.callsArgWith(1, response_from_ignition).returns(new PassThrough());
        function callback(data){

            expect(data).to.be.an('array');
            expect(data.length).to.equal(0);

            done();
        }
        alarms.alarms_for_public_table(callback); 
    });
    it("doesn't crash when ignition doesn't send a building in the tag", function(done) {
        var options = {};
        options['who'] = 'blah blah blah';
        var tag = IGNITION_TAG(options);
        var tags = [tag];
        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.get.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        alarms.alarms_for_public_table(function(result) {
            expect(result.tags.length).to.equal(0);

            done();
        });
    });

    it("only_escalated_tags should appear on table", function(done) {
        var tags = [];
        var options = {};

        options['who'] = "aca/escalate/constraint0";
        options['escalated'] = true;
        tag = IGNITION_TAG(options);
        tags.push(tag);

        options['who'] = "bhd/syserr/constraint0";
        options['escalated'] = true;
        tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.get.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(data){
            expect(data).to.be.an('object');
            expect(data.tags).to.be.an('array');
            expect(data.tags.length).to.equal(1);
            expect(data.tags[0].who.toLowerCase().indexOf('escalated')).to.equal(-1);
            expect(data.tags[0].escalated).to.be.true;

            done();
        }
        alarms.alarms_for_public_table(callback);
    });

    it("all building names are upper case", function(done) {
        var tags = [];
        var options = {};

        options['who'] = "aca/Escalate/constraint0";
        options['alarming'] = true;
        options['escalated'] = true;
        tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var expected_from_alarms = tags_from_ignition;
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(expected_from_alarms));
        response_from_ignition.end();

        this.get.callsArgWith(1, response_from_ignition).returns(new PassThrough());

        function callback(data){
            expect(data.tags[0].who.split('/')[0]).to.equal('ACA');

            done();
        }
        alarms.alarms_for_public_table(callback);
    });

    it("power outage count should not include alarms that have recently cleared", function(done) {
        var when_alarmed = Date.now();
        var when_cleared = when_alarmed - (11 * 60 * 1000);


        var tags = [];
        var options = {};

        options['who'] = "aca/Escalate/constraint0";
        options['alarming'] = true;
        options['when_alarmed'] = new Date(when_alarmed).toString()
        tag = IGNITION_TAG(options);
        tags.push(tag);

        options['who'] = "llc/Escalate/constraint0";
        options['alarming'] = false;
        options['when_cleared'] = new Date(when_cleared).toString()
        tag = IGNITION_TAG(options);
        tags.push(tag);

        var tags_from_ignition = {rows:tags, shelved_tags:[]};
        var response_from_ignition = new PassThrough();
        response_from_ignition.write(JSON.stringify(tags_from_ignition));
        response_from_ignition.end();

        this.get.callsArgWith(1, response_from_ignition);

        function callback(data){
            debugger;
            expect(data.tags[0].who.split('/')[0]).to.equal('ACA');
            expect(data.tags[0].recently_changed).to.be.true;

            done();
        }
        debugger;
        alarms.alarms_for_public_table(callback);
    });
});